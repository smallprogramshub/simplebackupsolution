# My Simple Backup Solution

I created this script to backup my folders into a 7z file. With this, I'm able to create a checksum file that'll let me know if any of my files are corrupted.
It's also a technical test of my bash scripting beginnings.