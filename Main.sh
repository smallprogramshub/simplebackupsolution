today=$(date +'%m.%d.%Y') &&

echo "Welcome to my backup solution. This will create 7z files of your home folders & move them to your requested destination." 
echo "In addition, a sha256 hash file will be created for you to verify the folder's integrity."
echo "Make sure you have adequate storage, a copy of your files will be created on the drive."
echo
echo "In the future, I'll add a text file so you can add folder directories you want backed up automatically so you won't have to input it everytime."
echo
echo "Your document folder, photo folder and video folder will be automatically transferred"
read -p "Enter the full directory that'll hold the backed up files: " HD_Var
read -p "Enter the entire file directory for your SD Card folder" SD_Var

#Creates a folder with today's date on the external drive
cd HD_Var &&
mkdir $today &&
cd $today &&
mkdir PC &&
mkdir EXTERNAL &&
cd $HOME &&
echo "\

Creating Documents 7z....

" &&
7z a Documents $HOME'/Documents' &&
echo "\

Copying Documents....

" &&
cp Documents.7z $HD_Var'/'$today'/PC' &&
echo "\

Documents Transferred Successfully

" &&

echo "\

Creating Pictures 7z....

" &&

7z a Pictures $HOME'/Pictures' &&

echo "\

Copying Pictures....

" &&
cp Pictures.7z $HD_Var'/'$today'/PC' &&
echo "\

Pictures Transferred Successfully

" &&

echo "\

Creating Videos 7z....

" &&

7z a Videos $HOME'/Videos' &&

echo "\

Copying Videos....

" &&

cp Videos.7z $HD_VAR'/'$today'/PC' &&
echo "\

Videos Transferred Successfully

" &&

rm Videos.7z Documents.7z Pictures.7z &&
echo "\

Files Deleted

" &&

cd $HD_VAR'/'$today'/PC' &&
echo "\

Making SHA256 hashes... please wait...

" &&

sha256sum -b * > SHA256SUM &&

echo "\

Backup complete!

"